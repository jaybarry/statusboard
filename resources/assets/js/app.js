var React = require('react');
var ReactDOM = require('react-dom');

import Weather from './components/Weather';
import Slack from './components/Slack';
import GoogleCalendar from './components/GoogleCalendar';
import Beer from './components/Beer';
import SlackMessage from './components/SlackMessage';

ReactDOM.render(
		(
			<div className="content-wrapper">
				<Weather url="/api/weather" pollInterval={300000} />
				<Slack url="/api/slackusers" pollInterval={300000} />
				<GoogleCalendar url="/api/googlecalendar" pollInterval={300000} />
				<Beer url="/api/beer/8384,12693" pollInterval={3000000} />
				<SlackMessage url="/api/slackmessage" pollInterval={300000} />
			</div>
		),
		document.getElementById('content')
	);

