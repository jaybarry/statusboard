var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var $ = require('jquery');


var GoogleCalendar = React.createClass ({

  loadEventsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },
  getInitialState: function() {
    return {
      data: []
    };
  },

  componentDidMount: function() {
    this.loadEventsFromServer();
    setInterval(this.loadEventsFromServer, this.props.pollInterval);
  },

  render: function() {

    var calendar = this.state.data.map( function(event,i) {

        return(
            <li className="events-item" key={i}>
              <time>{event.date} @ {event.time}</time>
              <span className="event-title">{event.title}</span>
            </li>
        );  
      
      
    }.bind(this));

    return (
      
        <div className="calendar module module-calendar">
          <div className="module-inner">
            <div className="module-header">
              <h2 className="module-header-title">SOCO Events</h2>
            </div>
            <ul className="events"> 
              {calendar}
            </ul>
          </div>
        </div>

    );
  }


});

export default GoogleCalendar;
