var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var $ = require('jquery');


var Weather = React.createClass ({

  loadWeatherFromServer: function() {
    this.setState({loading: true});
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,

      success: function(data) {
        this.setState({loading: false});
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },
  getInitialState: function() {
    return {
      loading: false,
      data: []
    };
  },

  componentDidMount: function() {
    this.loadWeatherFromServer();
    setInterval(this.loadWeatherFromServer, this.props.pollInterval);
  },

  render: function() {

    var weather = this.state.data.map( function(item,i) {

      return(
          <div key={i} className={"weather-content " + item.weather.icon}>
            <div className="weather-temp">{item.weather.current_temperature}</div>
            <div className="weather-feelslike">Feels like {item.weather.feels_like}</div>
            <p className="weather-summary">{item.weather.summary}</p>
          </div>
      );

    }.bind(this));

    return (

      <div className="weather module module-weather">
        <div className="module-inner">
        
            <div className="module-header">
              <h2 className="module-header-title">Current Weather</h2>
            </div>
            
            {weather}
        
        </div>
      </div>

    );
  }


});

export default Weather;
