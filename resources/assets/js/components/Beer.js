var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var $ = require('jquery');


var Beer = React.createClass ({

  loadBeerFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },
  // loadBeerAmountFromServer: function() {
  //   $.ajax({
  //     url: this.props.beeramturl,
  //     dataType: 'json',
  //     cache: false,
  //     success: function(data) {
  //       this.setState({beeramtdata: data});
  //     }.bind(this),
  //     error: function(xhr, status, err) {
  //       console.error(this.props.url, status, err.toString());
  //     }.bind(this)
  //   });

  // },
  getInitialState: function() {
    return {
      beeramtdata: [],
      data: []
    };
  },

  componentDidMount: function() {
    this.loadBeerFromServer();
    //this.loadBeerAmountFromServer();
    //setInterval(this.loadBeerFromServer, this.props.pollInterval);
    //setInterval(this.loadBeerAmountFromServer, this.props.pollIntervalAmt);
  },

  render: function() {

    // var allamts;
    // var amts = this.state.beeramtdata.map(function(amt,i) {
    //      console.log(amt.full_level_oz);
    // });


    var beers = this.state.data.map( function(beer,i) {

     
        if(i===0) {
            var tap = 'Left';
        } else {
          var tap = 'Right';
        }

        //var beerrate = this.state.beeramtdata[i];
        //console.log(beerrate);

        return(
            <div className="beer" key={i}>
                


              

              <div className="beer-content">

              <figure><img src={beer.label} alt=""/></figure>

              <div className="beer-info">
                <h4 className="beer-tap">{tap}</h4>
                <h5 className="beer-title">{beer.name}</h5>
                <p className="beer-brewery">{beer.brewery}</p>
                <p className="beer-style">{beer.style}</p>
                <p className="beer-attributes">
                  <span className="beer-attribute">ABV: {beer.abv}%</span>
                  <span className="beer-attribute">IBU: {beer.ibu}</span>
                </p>

                
                                
              </div>
              </div>

            </div>
        );  
      
      
    }.bind(this));

    return (
      
        <div className="beers module module-beers">
          
          <div className="module-inner">
            <div className="module-header">
              <h2 className="module-header-title">On Tap</h2>
            </div>
            {beers}          
          </div>
        </div>


    );
  }


});

export default Beer;
