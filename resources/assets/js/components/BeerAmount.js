var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var $ = require('jquery');


var BeerAmount = React.createClass ({

  loadBeerFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },
  getInitialState: function() {
    return {
      data: []
    };
  },

  componentDidMount: function() {
    this.loadBeerFromServer();
    setInterval(this.loadBeerFromServer, this.props.pollInterval);
  },

  render: function() {

    var beers = this.state.data.map( function(beer,i) {

      console.log(i);
        if(i===0) {
            var tap = 'Left';
        } else {
          var tap = 'Right';
        }

        return(
            <div className="beer" key={i}>
                


              

              <div className="beer-content">

              <figure><img src={beer.label} alt=""/></figure>

              <div className="beer-info">
                <h4 className="beer-tap">{tap}</h4>
                <h5 className="beer-title">{beer.name}</h5>
                <p className="beer-brewery">{beer.brewery}</p>
                <p className="beer-style">{beer.style}</p>
                <p className="beer-attributes">
                  <span className="beer-attribute">ABV: {beer.abv}%</span>
                  <span className="beer-attribute">IBU: {beer.ibu}</span>
                </p>
              </div>
              </div>

            </div>
        );  
      
      
    }.bind(this));

    return (
      
        <div className="beers module module-beers">
          
          <div className="module-inner">
            <div className="module-header">
              <h2 className="module-header-title">On Tap</h2>
            </div>
            {beers}          
          </div>
        </div>


    );
  }


});

export default BeerAmount;
