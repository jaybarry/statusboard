var React = require('react');
var ReactDOM = require('react-dom');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var $ = require('jquery');


var SlackMessage = React.createClass ({

  loadMessageFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },
  getInitialState: function() {
    return {
      data: []
    };
  },

  componentDidMount: function() {
    
    this.loadMessageFromServer();
    setInterval(this.loadMessageFromServer, this.props.pollInterval);
    
  },

  componentWillUpdate: function() {
    var el = ReactDOM.findDOMNode(this);
    $(el).fadeIn('slow');
    
  },
  componentDidUpdate: function() {
      var el = ReactDOM.findDOMNode(this);
      setTimeout(function(){
         $(el).fadeOut('fast');
    
      }, 10000);
  },
 

  render: function() {

    var message = this.state.data.map( function(message,i) {

      
        return(
            <div className="message-content" key={i}>
              
              <div className="message-quote">
                <figure><img src={message.avatar}/></figure>


                <div className="message-quote-text">
                    <span className="quote">&ldquo;</span>
                      <p>{message.text}</p>
                    <span className="quote">&rdquo;</span>
                  </div>
                  
                </div>
            </div>
            
        );  
      
      
    }.bind(this));

    return (
      
        
                <div id="message" className="message">
                  {message}
                </div>



    );
  }


});

export default SlackMessage;
