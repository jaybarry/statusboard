var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var $ = require('jquery');


var Slack = React.createClass ({

  loadUsersFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },
  getInitialState: function() {
    return {
      data: []
    };
  },

  componentDidMount: function() {
    this.loadUsersFromServer();
    setInterval(this.loadUsersFromServer, this.props.pollInterval);
  },

  render: function() {

    var users = this.state.data.map( function(user,i) {

      if(user.presence == 'active')
      {
        return(
            <img key={i} src={user.profile.image_72}/>
        );  
      }
      
    }.bind(this));

    return (
      
        <div className="users module module-users">
          <div className="module-inner">
            <div className="module-header">
              <h2 className="module-header-title">Active Slack Users</h2>
            </div>
            <div className="module-content thumbnails">
              {users}
            </div>
          </div>
        </div>


    );
  }


});

export default Slack;
