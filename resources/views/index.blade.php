<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Status</title>

    <script src="https://use.typekit.net/huw3cvp.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <link href="{{ url('/css/styles.css') }}" rel="stylesheet">


</head>

<body>

<div class="images">
    <div class="image image1"></div>
    <div class="image image2"></div>
    <div class="image image3"></div>
    <div class="image image4"></div>
</div>

<div class="site">
    <div class="header-wrapper wrapper">
	    <header class="header">
		    <h1 class="logo">SOCO Statusboard v1</h1>
	   	</header>

        <div class="wifi">
        <p>Network: <strong>soco-guest</strong>  Password: <strong>hellosoco</strong></p>
        </div>


   </div>

    <div id="content"></div>
</div>

    <script src="{{ url('/js/app.js') }}"></script>

</body>
</html>
