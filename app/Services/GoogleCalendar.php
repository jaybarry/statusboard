<?php namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class GoogleCalendar {

    protected $client;

    protected $service;

    function __construct() {
        /* Get config variables */
        $client_id = env('GOOGLE_APP_ID');
        $service_account_name = env('GOOGLE_APP_EMAIL');
        $key_file_location = base_path() . env('GOOGLE_APP_P12');

        $this->client = new \Google_Client();
        $this->client->setClassConfig('Google_Cache_File', array('directory' => storage_path().'/framework/cache'));
        $this->client->setApplicationName("Statusboard");
        $this->service = new \Google_Service_Calendar($this->client);

        if (Cache::has('service_token')) {
          $this->client->setAccessToken(Cache::get('service_token'));
        }

        $key = file_get_contents($key_file_location);

        $scopes = array('https://www.googleapis.com/auth/calendar');
        $cred = new \Google_Auth_AssertionCredentials(
            $service_account_name,
            $scopes,
            $key
        );

        $this->client->setAssertionCredentials($cred);
        if ($this->client->getAuth()->isAccessTokenExpired()) {
          $this->client->getAuth()->refreshTokenWithAssertion($cred);
        }
        Cache::forever('service_token', $this->client->getAccessToken());
    }

    public function getEvents($calendarId,$params=null)
    {
    	$results = $this->service->events->listEvents($calendarId,$params);
        return $results;
    }


}