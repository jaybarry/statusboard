<?php

namespace App\Http\Controllers;

use Frlnc\Slack\Http\SlackResponseFactory;
use Frlnc\Slack\Http\CurlInteractor;
use Frlnc\Slack\Core\Commander;

use App\Services\GoogleCalendar;
use Carbon\Carbon;


use Cache;

class ApiController extends Controller
{

    public function getCurrentWeather()
    {

      $overcast = new \VertigoLabs\Overcast\Overcast('fb8c0c7729b0cfe3ef111a8acb83660e', new \VertigoLabs\Overcast\ClientAdapters\FileGetContentsClientAdapter());
      $forecast = $overcast->getForecast(34.0000278,-81.0412823);

      $weather['weather']['current_temperature'] = ceil($forecast->getCurrently()->getTemperature()->getCurrent());
      $weather['weather']['feels_like'] = ceil($forecast->getCurrently()->getApparentTemperature()->getCurrent());
      $weather['weather']['summary'] = $forecast->getDaily()->getSummary();
      $weather['weather']['icon'] = $forecast->getCurrently()->getIcon();


      return response()->json(array($weather));
    }

    //C04BFJTNA,C04BFJTNJ

    public function getSlackMessages($channel)
    {

      $token = Cache::get('slack_token');

      if(isset($token)) {
      

        $interactor = new CurlInteractor;
        $interactor->setResponseFactory(new SlackResponseFactory);

        $commander = new Commander($token, $interactor);

        $response = $commander->execute('channels.history', [
            'channel' => $channel,
            'count' => 50
        ]);
        
       
        return $response->getBody();

      }


    }

    public function getSlackUser($userid)
    {

      $token = Cache::get('slack_token');

      if(isset($token)) {
      

        $interactor = new CurlInteractor;
        $interactor->setResponseFactory(new SlackResponseFactory);

        $commander = new Commander($token, $interactor);

        $response = $commander->execute('users.info', [
            'user' => $userid
        ]);
        
       
        return $response->getBody();

      }


    }

    public function getRandomSlackMessage()
    {

      $general = $this->getSlackMessages('C04BFJTNA');
      $random = $this->getSlackMessages('C04BFJTNJ');

      

      $all = array_merge($general['messages'],$random['messages']);





      foreach($all as $k => $message) {

        if(!isset($message['subtype']) && !isset($message['attachments']) && ($message['text'] != '')) {
         
          $find = strpos($message['text'],'<');
          if($find === false) {
            $filtered[] = $message;
          }

        }
      }

      $random = array_rand($filtered,1);

      

      $message = $filtered[$random];

      $user = $this->getSlackUser($message['user']);
      $message['name'] = $user['user']['real_name'];
      $message['avatar'] = $user['user']['profile']['image_192'];
      // echo "<pre>";
      // print_r($message);
      // echo "</pre>";



      return response()->json( array($message) );


    }

        public function getSlackUsers()
    {


      $token = Cache::get('slack_token');

      if(isset($token)) {
      

        $interactor = new CurlInteractor;
        $interactor->setResponseFactory(new SlackResponseFactory);

        $commander = new Commander($token, $interactor);

        $response = $commander->execute('users.list', [
            'presence' => 'active'
        ]);
        
        $users = $response->getBody()['members'];

        return response()->json( $users );

      }



    }

    public function getGoogleCalendar(GoogleCalendar $calendar)
    {

      $token = Cache::get('googlecalendar_token');
      
      if(isset($token)) {

          $carbon = new Carbon('now','America/new_york');
          $formatted = $carbon->toRfc3339String();

          $params = array(
              'maxResults' => 10,
              'timeMin' => $formatted,
              'singleEvents' => true,
              'orderBy' => 'startTime'
          );

          $calendarId = "706fpigq7h1usa4cfikn309ddk@group.calendar.google.com";
          $result = $calendar->getEvents($calendarId,$params);

          foreach ($result->getItems() as $k => $event) {

            $datetime = $event->getStart()->dateTime;

            $events[$k]['title'] = $event->getSummary();
            if($datetime) {

              $date = Carbon::createFromTimestamp( strtotime($datetime) , 'America/new_york' );

              $events[$k]['date'] = $date->format('F j');
              $events[$k]['time'] = $date->format('g:i a');

              //dd(Carbon::)

            }
            
          }

        return response()->json( $events );

      }



    }

     public function getBeers($beerids)
    {

      if(explode(",",$beerids)) {
        $beerids = explode(",",$beerids);
      } else {
        $beerids = array($beerids);
      } 

      foreach($beerids as $k => $beerid) {

            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', 'https://api.untappd.com/v4/beer/info/'.$beerid.'/', [
                'query' => [
                  'client_id' => env('UNTAPPD_ID'),
                  'client_secret' => env('UNTAPPD_SECRET'),
                ],
            ]);
            

            $apibeers = json_decode($res->getBody());

            foreach($apibeers->response as $key => $beer) {
              
                $beers[$k]['name'] = $beer->beer_name;
                $beers[$k]['label'] = $beer->beer_label;
                $beers[$k]['abv'] = $beer->beer_abv;
                $beers[$k]['ibu'] = $beer->beer_ibu;
                $beers[$k]['brewery'] = $beer->brewery->brewery_name;
                $beers[$k]['style'] = $beer->beer_style;
                $beers[$k]['description'] = $beer->beer_description;

            }

      }
  
      return response()->json( $beers );
    }

    public function counter()
    {

      if(Cache::get('api_counter')) {
        Cache::forever('api_counter', Cache::get('api_counter') + 1);
      } else {
        Cache::forever('api_counter', 1);
      }

      return response()->json( Cache::get('api_counter') );
    }


     public function beerrate()
    {


            $client = new \GuzzleHttp\Client();

            $res = $client->request('GET', 'https://soco-kegerator.herokuapp.com/tap/tap-two', [
                'query' => [
                  'auth_token' => 'EbsiwhMTLyrp265i2sozK6QW',
                  'cache-control' => 'no-cache',
                  'content-type' => 'application/json'
                ],
            ]);

            $body1 = json_decode($res->getBody());

            $taps = array();


            $oz_remaining = $body1->full_level_oz-$body1->poured_oz;

            $pints_remaining = ceil( $oz_remaining/16 );

            $taps[0]['tap_name'] = $body1->tap_name;
            $taps[0]['full_level_oz'] = $body1->full_level_oz;
            $taps[0]['poured_oz'] = $body1->poured_oz;
            $taps[0]['name'] = $body1->name;
            $taps[0]['pints_remaining'] = $pints_remaining;


            $client = new \GuzzleHttp\Client();

            $res = $client->request('GET', 'https://soco-kegerator.herokuapp.com/tap/tap-one', [
                'query' => [
                  'auth_token' => 'EbsiwhMTLyrp265i2sozK6QW',
                  'cache-control' => 'no-cache',
                  'content-type' => 'application/json'
                ],
            ]);

            $body2 = json_decode($res->getBody());

            $oz_remaining = $body2->full_level_oz-$body2->poured_oz;

            $pints_remaining = ceil( $oz_remaining/16 );

            $taps[1]['tap_name'] = $body2->tap_name;
            $taps[1]['full_level_oz'] = $body2->full_level_oz;
            $taps[1]['poured_oz'] = $body2->poured_oz;
            $taps[1]['name'] = $body2->name;
            $taps[1]['pints_remaining'] = $pints_remaining;



            $taps[1]['remaing'] = $body2->name;

            

            //dd($body);
  
        return response()->json( $taps );
    }

}
