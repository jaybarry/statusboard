<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Contracts\Cache\Repository;

use Cache;


class AuthController extends Controller
{

    public function slack(Request $request)
    {

      		$provider = new \AdamPaterson\OAuth2\Client\Provider\Slack([
			    'clientId'          => env('SLACK_CLIENT_ID'),
			    'clientSecret'      => env('SLACK_CLIENT_SECRET'),
			    'redirectUri'       => url('auth/slack'),
			]);

      		$scopes = '&scope=' .urlencode('channels:read,users:read,users:info,channels:history,channels:list'); 

			if ( !$request->get('code') ) {

			    // If we don't have an authorization code then get one
			    $authUrl = $provider->getAuthorizationUrl().$scopes;

			    // dd($authUrl);
			    Cache::forever('oauth2state', $provider->getState() );

			     header('Location: '.$authUrl);

			    exit;

			// Check given state against previously stored one to mitigate CSRF attack
			} elseif ( empty($request->get('state')) || ( $request->get('state') !== Cache::get('oauth2state') )) {

				Cache::forget('oauth2state');
			    exit('Invalid state');

			} else {

				$code = $request->get('code');

			    // Try to get an access token (using the authorization code grant)
			    $token = $provider->getAccessToken('authorization_code', [
			        'code' => $code
			    ]);

			    Cache::forever('slack_token',$token->getToken());

			    // Optional: Now you have a token you can look up a users profile data
			    try {

			        // We got an access token, let's now get the user's details
			        $team = $provider->getResourceOwner($token);

			        // Use these details to create a new profile
			        printf('Hello %s!', $team->getName());

			    } catch (Exception $e) {

			        // Failed to get user details
			        exit('Oh dear...');
			    }

			    // Use this to interact with an API on the users behalf
			    echo $token->getToken();
			}

    }


    public function googlecalendar(Request $request)
    {

      		$provider = new \League\OAuth2\Client\Provider\Google([
			    'clientId'          => env('GOOGLE_APP_ID'),
			    'clientSecret'      => env('GOOGLE_APP_SECRET'),
			    'redirectUri'       => url('/auth/googlecalendar'),
			    'hostedDomain' 		=> url('/'),
			]);

      		//$scopes = '&scope=' .urlencode('channels:read,users:read'); 

			if ( !$request->get('code') ) {

			    // If we don't have an authorization code then get one
			    $authUrl = $provider->getAuthorizationUrl();
			    //dd($authUrl);

			    // dd($authUrl);
			    Cache::forever('google_oauth2state', $provider->getState() );

			    header('Location: '.$authUrl);

			    exit;

			// Check given state against previously stored one to mitigate CSRF attack
			} elseif ( empty($request->get('state')) || ( $request->get('state') !== Cache::get('google_oauth2state') )) {

				Cache::forget('google_oauth2state');
			    exit('Invalid state');

			} else {

				$code = $request->get('code');

			    // Try to get an access token (using the authorization code grant)
			    $token = $provider->getAccessToken('authorization_code', [
			        'code' => $code
			    ]);

			    Cache::forever('googlecalendar_token',$token->getToken());

			    // Optional: Now you have a token you can look up a users profile data
			    try {

			        // We got an access token, let's now get the user's details
			        //$team = $provider->getResourceOwner($token);
			    	dd($provider->getResourceOwner($token));
			        // Use these details to create a new profile
			        //printf('Hello %s!', $ownerDetails->getFirstName());

			    } catch (Exception $e) {

			        // Failed to get user details
			        exit('Oh dear...');
			    }

			    // Use this to interact with an API on the users behalf
			    echo $token->getToken();
			}

    }

   

}
