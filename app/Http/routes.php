<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'Controller@index');

//Auth
$app->get('/auth/slack', 'AuthController@slack');
$app->get('/auth/googlecalendar', 'AuthController@googlecalendar');
$app->get('/auth/untappd', 'AuthController@untappd');


//API
$app->get('/api/weather', 'ApiController@getCurrentWeather');
$app->get('/api/slackusers', 'ApiController@getSlackUsers');
$app->get('/api/slackmessage', 'ApiController@getRandomSlackMessage');
$app->get('/api/slackuser', 'ApiController@getSlackUser');
$app->get('/api/googlecalendar', 'ApiController@getGoogleCalendar');
$app->get('/api/beer/{beers}', 'ApiController@getBeers');
$app->get('/api/untappd/{beers}', 'ApiController@unTappd');
$app->get('/api/counter', 'ApiController@counter');
$app->get('/api/beerrate', 'ApiController@beerrate');
