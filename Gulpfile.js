var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.browserify('app.js');
    mix.sass('styles.scss');

    mix.browserSync({
        proxy: 'statusboard.dev'
    });
});
